var back_action = false;
window.addEventListener('load', function(event) { automata_init(); });

function automata_control_add(control, servret) {
    let form = control.getAttribute('data-automata-form');
    let action = control.getAttribute('data-automata-action');
    let updtarea = control.getAttribute('data-automata-updtarea');
    let index = control.getAttribute('data-automata-index');
    automata_listener_add(
        'click', updtarea, index, control.id, form, action, servret);
}


function automata_init() {
    let buttons = document.getElementsByTagName('button');
    for (let button of buttons)
        automata_control_add(button, true);
    
    let links = document.getElementsByTagName('a');
    for (let link of links)
        automata_control_add(link, false);
    if (!back_action)
        return;
    history.pushState(null, null, '');
    back_action = false;
}
        
function automata_back_listener_add(updtarea, target, formname) {
    window.onpopstate = function(event) {
        back_action = true;
        automata_request(event, updtarea, target, formname, 'back', true);
        history.replaceState(null, null, '');
    }
}

function automata_response(servret, updtarea, respcontent) {
    if (!servret)
        return;
    if (respcontent.readyState == 4 && respcontent.status == 200) {
        document.getElementById(updtarea).innerHTML = respcontent.responseText;
        automata_init();
    }
}

function automata_request(event, updtarea, target, formname, action, servret) {
    event.preventDefault();
    
    let form = new FormData();
    let inputs = document.getElementsByTagName('input');
    for (let input of inputs) {
       switch (input.type) {
       case 'checkbox':
          form.append(input.id, input.checked);
          break;
       case 'file':
          form.append(input.id, input.files[0]);
          break;
       default:
          form.append(input.id, input.value);
          break;
       }
    }
    inputs = document.getElementsByTagName('select');
    for (input of inputs)
       form.append(input.id, input.value);
    form.append('action', action);
    
    let request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        automata_response(servret, updtarea, this);
    };
    request.open('POST', target, /* async = */ false);
    request.send(form);
}

function automata_listener_add(
        event, updtarea, target, cntrlnm, formname, action, servret) {
    let control = document.getElementById(cntrlnm);
    control.addEventListener(event, function(event) {
        automata_request(event, updtarea, target, formname, action, servret);
    });
}
