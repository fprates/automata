package org.quanticsoftware.automata.application;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class DataObject {
	private String name, path, parent;
	private Object value;
	private DataType type;
	private DataContext datactx;
	private Map<String, String> items, aliases, ns;
	private Map<Object, String> indexes;
	private boolean initialized;
	private ParentContext parentctx, localctx;
	
	public DataObject(
			DataContext datactx,
			DataType type,
			ParentContext parentctx,
			String name,
			String value) {
		this.type = type;
		this.name = name;
		this.value = value;
		this.datactx = datactx;
		this.parentctx = parentctx;
		
		if (parentctx == null) {
			path = name;
		} else {
			if (parentctx.object.getType().isCollection() && (name == null)) {
				var index = parentctx.object.getItems().size();
				this.name = Integer.toString(index);
			}
			this.parent = parentctx.object.getPath();
			path = getFullName(this.parent, this.name);
		}
		
		datactx.set(path, this);
		ns = new HashMap<>();
		namespace(this);
		
		aliases = new HashMap<>();
		indexes = new HashMap<>();
		items = new LinkedHashMap<>();
		
		localctx = new ParentContext(this);
		localctx.action = (a)->initialize();
		
		for (var key : type.getItems()) {
			items.put(key, getFullName(path, key));
			if (!type.isAllocable(key))
				continue;
			
			var object = datactx.instance(type.get(key), localctx, key);
			alias(object, key);
		}
	}
	
	private final void alias(DataObject object, String name) {
		this.aliases.put(name, object.getPath());
		for (var key : object.getItems())
			alias(object.getItem(key), name.concat(".").concat(key));
	}
	
	private final void fail(String text) {
		throw new GeneralException(text);
	}
	
	@SuppressWarnings("unchecked")
	public final <T> T get() {
		return (T)value;
	}
	
	public final <T> T get(String name) {
		var item = getItem(name);
		return (item == null)? null : item.get();
	}
	
	public final boolean getbl() {
	    return (value == null)? false : (boolean)value;
	}
	
	public final boolean getbl(String name) {
		return getItem(name).getbl();
	}
	
	public final double getd() {
		return (value == null)? 0 : (double)value;
	}
	
	public final double getd(String name) {
		var value = get(name);
		return (value == null)? 0 : (double)value;
	}
	
	public final float getf(String name) {
		var value = get(name);
		return (value == null)? 0 : (float)value;
	}
	
	public final int geti() {
		return (value == null)? 0 : (int)value;
	}
	
	public final int geti(String name) {
		var value = get(name);
		return (value == null)? 0 : (int)value;
	}
	
	private final String getFullName(String parent, String name) {
		return new StringBuilder(parent).append(".").append(name).toString();
	}
	
	public final DataObject getItem(Object id) {
		if (type.getKey() != null)
			id = indexes.containsKey(id)? indexes.get(id) : id;
		var item = items.get((String)id);
		if (item == null)
			item = aliases.get((String)id);
		return datactx.get(item);
	}
	
	public final Set<String> getItems() {
		return items.keySet();
	}
	
	public final long getl(String name) {
		var value = get(name);
		return (value == null)? 0l : (long)value;
	}
	
	public final String getName() {
		return name;
	}
	
	public final Map<String, String> getNS() {
		return ns;
	}
	
	public final String getParent() {
		return parent;
	}
	
	public final String getPath() {
		return path;
	}
	
	public final String getst() {
	    return (value == null)? null : (String)value;
	}
	
	public final String getst(String name) {
		Object value = get(name);
		return (value == null)? null : (String)value;
	}
	
	public final DataType getType() {
		return type;
	}
    
	private final void initialize() {
		initialized = true;
		if (parentctx != null)
			parentctx.action.execute("initialized");
	}
	
	public final DataObject instance() {
		return instance(null);
	}
	
	public final DataObject instance(Object keyvalue) {
		if (type.getCollectionType() == 0)
			fail("set(index, data) allowed for collection only.");
		
		var key = type.getKey();
		if ((key != null) && (keyvalue == null))
			fail("index value for a map is required.");

		var item = datactx.instance(type.getElement(), localctx);
		var name = item.getName();
		items.put(name, item.getPath());
		
		if (key != null) {
			item.set(key, keyvalue);
			indexes.put(keyvalue, name);
		}
		
		return item;
	}
	
	public final boolean isInitialized() {
		return initialized;
	}
	
	public final Set<Object> keySet() {
		return indexes.keySet();
	}
	
	private final void namespace(DataObject object) {
		if (object == null)
			return;
		var type = object.getType();
		var stype = type.getSuperType();
		var tname = (stype == null)? type.getPath() : stype.getPath();
		ns.put(tname, object.getPath());
		var parent = object.getParent();
		if (parent != null)
			namespace(datactx.get(parent));
	}
	
	public final void set(Object value) {
		this.value = value;
		initialized = true;
		if (parentctx != null)
			parentctx.action.execute("initialize");
	}
	
	public final void set(String name, DataObject value) {
		var parent = items.get(name);
		datactx.set(parent, value);
		var tchild = type.get(name);
		for (var key : tchild.getItems())
			datactx.set(getFullName(parent, key), value.getItem(key));
	}
	
	public final void set(String name, Object value) {
		if (type.isAllocable(name))
			getItem(name).set(value);
		else
			set(name, (DataObject)value);
	}
    
//    public final void setn(double factor, String unit, double exp) {
//        value = new Operand(factor, unit, exp);
//    }
//    
//    public final void setn(String name, double factor, String unit, double exp)
//    {
//    	getItem(name).set(new Operand(factor, unit, exp));
//    }
    
	@Override
	public final String toString() {
		int i = 0;
		var sb = new StringBuilder(name).append("(").
		        append(type.getName()).append(")=").
		        append((this.value == null)? "null" : this.value.toString()).
		        append(" {");
		for (var key : items.keySet()) {
			if (i++ > 0)
				sb.append(", ");
			sb.append(key).append("=");
			var item = getItem(key);
			if (item == null) {
				sb.append("null");
				continue;
			}
			sb.append(item.toString());
		}
		return sb.append("}").toString();
	}
}
