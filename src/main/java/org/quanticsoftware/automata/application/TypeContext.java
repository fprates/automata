package org.quanticsoftware.automata.application;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TypeContext {
	private SharedTypeContext shtypectx;
	private static final int ARRAY = 1;
	private static final int MAP = 2;
	
	public TypeContext() {
		shtypectx = new SharedTypeContext();
		define("any");
		define("boolean");
		define("double");
		define("int");
		define("long");
		define("string");
		define("numop");
		define("date");
		define("time");
		define("type");
	}
	
	public final DataType array(DataType type, String name) {
		var collection = define(name);
		collection.setCollection(ARRAY, type);
		return collection;
	}
	
	public final DataType map(DataType type, String name, String key) {
		var collection = define(name, key);
		collection.setCollection(MAP, type);
		return collection;
	}
	
	public final DataType define(String name) {
		return define(null, name, null);
	}
	
	public final DataType define(String name, String key) {
		return define(null, name, key);
	}
	
	public final DataType define(DataType parent, String name, String key) {
		if (name == null)
			throw new GeneralException("type name can't be null.");
		var type = new DataType(shtypectx, parent, name, key);
		shtypectx.types.put(name, type);
		return type;
	}
	
	public final DataType get(String name) {
		return shtypectx.types.get(name);
	}
	
	public final Set<String> types() {
		return shtypectx.types.keySet();
	}
}

class SharedTypeContext {
	public Map<String, DataType> types;
	
	public SharedTypeContext() {
		types = new HashMap<>();
	}
}
