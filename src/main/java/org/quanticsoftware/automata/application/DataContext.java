package org.quanticsoftware.automata.application;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DataContext {
	private Map<String, DataObject> objects;
	private Map<String, Set<String>> references;
	private Map<String, Long> typecounters;
	
	public DataContext() {
		objects = new HashMap<>();
		references = new HashMap<>();
		typecounters = new HashMap<>();
	}
	
	public final void copy(String name, DataObject object) {
		set(name, object);
		for (var key : object.getItems())
			copy(name.concat(".").concat(key), object.getItem(key));
	}
	
	public final Set<String> entries() {
		return objects.keySet();
	}
	
	public final DataObject get(String name) {
		return objects.get(name);
	}
	
	private final long getCounter(String name) {
		var counter = typecounters.containsKey(name)?
				typecounters.get(name) : 0;
		typecounters.put(name, counter + 1);
		return counter;
	}
	
	public final DataObject instance(DataType type) {
		var typename = type.getName();
		var counter = getCounter(typename);
		var name = new StringBuilder(typename).
				append(".").append(counter).toString();
		return instance(type, name);
	}
	
	public final DataObject instance(DataType type, String name) {
		return instance(type, null, name);
	}
	
	public final DataObject instance(
			DataType type,
			ParentContext parentctx,
			String name) {
		return new DataObject(this, type, parentctx, name, null);
	}
	
	public final DataObject instance(
			DataType type,
			ParentContext parentctx) {
		return new DataObject(this, type, parentctx, null, null);
	}
	
	public final Set<String> references(String type) {
		return references.get(type);
	}
	
	public final void set(String name, DataObject object) {
		if (object == null)
		    return;
		
		objects.put(name, object);
		var type = object.getType();
		var typename = type.getName();
		var reference = references.get(typename);
		if (reference == null) {
		    references.put(typename, reference = new HashSet<>());
		    references.put(type.getPath(), reference);
		}
		reference.add(name);
		references.put(type.getPath(), reference);
	}
}
