package org.quanticsoftware.automata.cauldron;

import org.quanticsoftware.automata.runtime.RuntimeContext;
import org.quanticsoftware.automata.runtime.RuntimeInit;
import org.quanticsoftware.automata.runtime.Target;

public class BuildContext {
	public Target target;
	public RuntimeInit rtinit;
	public RuntimeContext rtctx;
	public int pinlevel, resultlimit;
}
