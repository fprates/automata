package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.runtime.Test;

public class BuildSpec {
	public BuildContext buildctx;
	public ContextPool ctxpool;
	public Map<String, List<String[]>> templates;
	public Map<String, Map<String, SpecFunction>> facilities;
	public Map<String, String> pinputs;
	public Set<String> branches, consumed;
	public BuildItem reference;
	public int clevel, programid, resultc;
	public boolean levellimit;
	public Test test;
	public Map<String, CauldronProgram> validated;
	
	public BuildSpec() {
		ctxpool = new ContextPool();
		templates = new HashMap<>();
		facilities = new HashMap<>();
		branches = new HashSet<>();
		consumed = new HashSet<>();
		pinputs = new HashMap<>();
		validated = new HashMap<>();
	}
}
