package org.quanticsoftware.automata.cauldron;


public class GetParameterName {
	
	public static final String run(String facility, String function, String key)
	{
	    return new StringBuilder(facility).append(".").
	            append(function).append(":").append(key).toString();
	}
	
}
