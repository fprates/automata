package org.quanticsoftware.automata.cauldron;

import java.util.HashSet;
import java.util.Set;

import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.GetLeastType;

public class ContextPool {
    public ObjectConnectionPool cpool;
    
    public ContextPool() {
        cpool = new ObjectConnectionPool();
    }

    public final String connect(String facility, String function,
            String direction, String dname, String dtype) {
		var name = new StringBuilder(facility).append(".").
		        append(function).append(":").
		        append(dname).toString();
		var connection = connectionInstance(name);
		connection.facility = facility;
		connection.function = function;
		connection.parameter = dtype;
		connection.direction = direction;
		connectionCompile(name);
		return name;
    }
    
    private final void connectionCompile(String name) {
        cpool.compile(name);
    }
    
    public final ObjectConnection connectionInstance(String name) {
        return cpool.instance(name);
    }
    
    public final Set<ObjectConnection> getConnections(String name) {
        return cpool.get(name);
    }
    
    public final Set<String> getInstances(String name) {
        Set<String> instances;
        var conns = cpool.get(name);
        
        if (conns == null)
            return null;
        instances = null;
        for (ObjectConnection conn : conns) {
            if (conn.function != null)
                continue;
            if (instances == null)
                instances = new HashSet<>();
            instances.add(conn.name);
        }
        return instances;
    }

    public final String instance(String name, DataObject object) {
		var params = new InstanceParameters();
		params.object = object;
		params.iname = name;
		return instance(params);
    }

    public final String instance(InstanceParameters params) {
		var oconn = cpool.instance(params.iname);
		oconn.parameter = GetLeastType.get(params.object.getType()).getPath();
		cpool.compile(params.iname);
		return params.iname;
    }
}
