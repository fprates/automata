package org.quanticsoftware.automata.cauldron;

public class CauldronProgram {
	public CauldronProgramContext context;
	
	public CauldronProgram(CauldronProgramContext context) {
		this.context = context;
		for (var item : context.items) {
			item.program = this;
			var reference = (item.reference >= 0)?
					context.items[item.reference] : item;
			item.output = getFunctionOutput(item, reference);
			if (reference != item)
				reference.input.get(item.source).object = item.output;
		}
	}
	
	private final String getFunctionOutput(
			ProgramItem item, ProgramItem reference) {
		var base = GetParameterName.run(
				reference.facility,
				reference.function,
				(item.source == null)? "output" : item.source);
		return new StringBuilder(base).append(".").
				append(reference.index).toString();
	}
	
	@Override
	public final String toString() {
		var sb = new StringBuilder("id: ").append(context.name).append("\n");
		for (int i = context.items.length - 1; i >= 0; i--)
			sb.append(context.items[i].toString()).append(";\n");
		return sb.toString();
	}
}
