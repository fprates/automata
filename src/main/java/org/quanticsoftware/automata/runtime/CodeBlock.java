package org.quanticsoftware.automata.runtime;

import java.util.Stack;

import org.quanticsoftware.automata.application.DataObject;

public class CodeBlock {
	public Stack<String> keys;
	public DataObject items;
	public int index;
	public boolean leave;
}
