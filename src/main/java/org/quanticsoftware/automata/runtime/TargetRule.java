package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.application.DataObject;

public interface TargetRule {
	
	public abstract boolean isValid(Test test, DataObject document);
	
}

