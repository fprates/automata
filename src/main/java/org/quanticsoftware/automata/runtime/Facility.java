package org.quanticsoftware.automata.runtime;

import java.util.Set;

import org.quanticsoftware.automata.application.DataObject;

public interface Facility {
	
	public abstract Function function(String name);
	
	public abstract Set<String> functions();
	
	public abstract String getAbsoluteName(String function);
	
	public abstract DataObject run(String function, SharedSession shsession);
	
}

