package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.application.DataObject;

public class InputGet {

	public static final Map<String, DataObject> execute(InputData idata) {
		var input = new HashMap<String, DataObject>();
		for (var pinput : idata.program.context.input.keySet()) {
			var object = idata.objects.get(pinput);
			if (object != null)
				input.put(pinput, object);
		}

		return input;
	}
}