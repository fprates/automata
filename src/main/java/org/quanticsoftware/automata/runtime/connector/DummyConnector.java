package org.quanticsoftware.automata.runtime.connector;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.DataType;

public class DummyConnector implements Connector {
    private Map<String, Map<Object, DataObject>> database;
    
    public DummyConnector() {
    	database = new HashMap<>();
    }
    
    @Override
    public final void create(DataType type) {
    	database.put(type.getName(), new HashMap<>());
    }
    
    @Override
    public final Set<String> getDocuments() {
        return database.keySet();
    }
    
    @Override
    public final DBSession instance(DataContext context) {
        return new DummySession(database);
    }
}
