package org.quanticsoftware.automata.runtime.connector;

import java.util.HashSet;
import java.util.Set;

import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataType;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoConnector implements Connector {
	private MongoClient dbclient;
	private MongoDatabase db;
	
	public MongoConnector(String host, int port, String db) {
		dbclient = new MongoClient(host, port);
		this.db = dbclient.getDatabase(db);
	}
	
	@Override
	public final void create(DataType type) {
		db.createCollection(type.getPath());
	}
	
	@Override
	public final Set<String> getDocuments() {
		Set<String> documents = new HashSet<>();
		for (String key : db.listCollectionNames())
			documents.add(key);
		return documents;
	}
	
	@Override
	public final DBSession instance(DataContext context) {
		return new MongoDBSession(context, db, dbclient.startSession());
	}
}
