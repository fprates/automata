package org.quanticsoftware.automata.runtime.connector;

import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.DataType;

public interface DBSession {
	
	public abstract void close();
	
	public abstract void commit();
	
	public abstract DataObject get(DataType type, Object key);
	
	public abstract void insert(DataObject data);
	
	public abstract boolean isTransactionStarted();
	
	public abstract void rollback();
	
	public abstract void save(DataObject data);
	
	public abstract void start();
	
	public abstract void update(Query query);
	
	public abstract void update(DataObject data);
	
}