package org.quanticsoftware.automata.runtime.connector;

import java.util.ArrayList;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.DataType;

import com.mongodb.MongoCommandException;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

public class MongoDBSession implements DBSession {
	private ClientSession session;
	private MongoDatabase db;
	private DataContext datactx;
	
	public MongoDBSession(
			DataContext datactx, MongoDatabase db, ClientSession session) {
		this.db = db;
		this.session = session;
		this.datactx = datactx;
	}
	
	@Override
	public final void close() {
		session.close();
	}
	
	@Override
	public final void commit() {
		session.commitTransaction();
	}
	
	@SuppressWarnings("unchecked")
	private final <T> T convert(DataType type, Document document) {
		DataObject data = datactx.instance(type, type.getName());
		
		var typekey = type.getKey();
		for (var key : type.getItems()) {
			var child = type.get(key);
			var value = (key.equals(typekey))?
					document.get("_id") : document.get(key);
			if (child.getItems().size() == 0)
				data.set(key, value);
			else
				data.set(key, convert(child, (Document)value));
		}
		return (T)data;
	}
	
	@Override
	public final DataObject get(DataType type, Object key){
		var collection = db.getCollection(type.getName());
		var document = collection.find(Filters.eq("_id", key)).first();
		return (document == null)? null : convert(type, document);
	}
	
	@Override
	public final void insert(DataObject data) {
		var collname = data.getType().getPath();
		var collection = db.getCollection(collname);
		collection.insertOne(session, instance(data));
	}
	
	@SuppressWarnings("unchecked")
	private final <T> T instance(DataObject data) {
		var type = data.getType();
		var key = type.getKey();
		var items = type.getItems();
				
		if (items.size() == 0)
			return data.get();

		var document = (key == null)?
				new Document() : new Document("_id", data.get(key));
		for (var ikey : items) {
			if (ikey.equals(key))
				continue;
			if (type.isAllocable(ikey)) {
				document.append(ikey, instance(data.getItem(ikey)));
				continue;
			}
			var item = data.getItem(ikey);
			var itemkey = item.getType().getKey();
			document.append(ikey, item.get(itemkey));
		}
		return (T)document;
	}

	@Override
	public boolean isTransactionStarted() {
		return session.hasActiveTransaction();
	}
	
	@Override
	public final void rollback() {
		session.abortTransaction();
	}
	
	@Override
	public final void save(DataObject data) {
		var type = data.getType();
		for (var key : data.getItems())
			if (!type.isAllocable(key))
				save(data.getItem(key));
		var document = (Document)instance(data);
		var collname = type.getName();
		var collection = db.getCollection(collname);
		
		try {
			collection.insertOne(session, document);
		} catch (MongoCommandException e) {
			var filter = Filters.eq("_id", data.get(type.getKey()));
			collection.updateOne(session, filter, document);
		}
	}
	
	@Override
	public final void start() {
		session.startTransaction();
	}
	
	@Override
	public final void update(Query query) {
		String field;
		var type = query.getType();
		var typekey = type.getKey();
		var typename = type.getName();
		var collection = db.getCollection(typename);
		var values = query.getValues();
		var changeset = new Bson[values.size()];
		Bson filter = null;
		int i = 0;
		
		for (var key : values.keySet())
			changeset[i++] = Updates.set(key, values.get(key));
		
		for (var clause : query.getWhere()) {
			field = (field = clause.getField()).equals(typekey)? "_id" : field;
			filter = Filters.eq(field, clause.getValue());
		}
		
		collection.updateOne(session, filter, Updates.combine(changeset));
	}
	
	@Override
	public final void update(DataObject data) {
		var type = data.getType();
		var collection = db.getCollection(type.getName());
		var fields = type.getItems();
		var changeset = new ArrayList<Bson>();
		var key = type.getKey();
		
		for (var field : fields)
			if (!field.equals(key))
				changeset.add(Updates.set(field, data.get(field)));
		
		collection.updateOne(session,
				Filters.eq("_id", data.get(type.getKey())),
				Updates.combine(changeset));
	}
	
}
