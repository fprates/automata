package org.quanticsoftware.automata.runtime;

public interface RuntimeInit {
	
	public abstract String init(RuntimeContext runtime);
	
}
