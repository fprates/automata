package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.application.Context;
import org.quanticsoftware.automata.application.DataContext;
import org.quanticsoftware.automata.application.DataType;
import org.quanticsoftware.automata.application.TypeContext;
import org.quanticsoftware.automata.cauldron.CauldronProgram;
import org.quanticsoftware.automata.output.engines.ButtonEngine;
import org.quanticsoftware.automata.output.engines.DataFormEngine;
import org.quanticsoftware.automata.output.engines.FormEngine;
import org.quanticsoftware.automata.output.engines.GenericEngine;
import org.quanticsoftware.automata.output.engines.RenderEngine;
import org.quanticsoftware.automata.output.engines.TextEngine;
import org.quanticsoftware.automata.output.engines.TextFieldEngine;
import org.quanticsoftware.automata.output.engines.TreeEngine;

public class RuntimeContext implements Context {
	private SharedRuntimeContext shrtctx;
	private TypeContext typectx;
	public InitConnector initconn;
	public Map<String, EventHandler> eventhandlers;
	public Map<String, Target> targets;
	public Map<String, CauldronProgram> programs;
	public PageSpec pagespectemplate;
	public PageConfig pageconfigtemplate;
	public DataContext datactx;
	public Map<String, RenderEngine> engines;
	public Map<String, String> outputpages;
	
	public RuntimeContext(SharedRuntimeContext shrtctx) {
		eventhandlers = new HashMap<>();
		targets = new HashMap<>();
		programs = new HashMap<>();
		typectx = new TypeContext();
		datactx = new DataContext();
		outputpages = new HashMap<>();
		this.shrtctx = shrtctx;
		
		engines = new HashMap<>();
		engines.put("body", new GenericEngine("body", GenericEngine.NO_ID));
		engines.put("button", new ButtonEngine());
		engines.put("container", new GenericEngine("div"));
		engines.put("dataform", new DataFormEngine());
		engines.put("form", new FormEngine());
		engines.put("head", new GenericEngine("head", GenericEngine.NO_ID));
		engines.put("meta", new GenericEngine("meta", GenericEngine.NO_ID));
		engines.put("node_list", new GenericEngine("ul"));
		engines.put("node_list_item", new GenericEngine("li"));
		engines.put("script", new GenericEngine("script", GenericEngine.NO_ID));
		engines.put("text", new TextEngine());
		engines.put("textfield", new TextFieldEngine());
		engines.put("tree", new TreeEngine());
		engines.put("view", new GenericEngine("html", GenericEngine.NO_ID));
	}
	
	public final Facility facility(String name) {
		var facility = shrtctx.facilities.get(name);
		if (facility == null)
			facility(name, facility = new CauldronFacility(name));
		return facility;
	}
	
	public final void facility(String name, Facility facility) {
		shrtctx.facilities.put(name, facility);
	}
	
	public final Page page(String name) {
		var page = new Page(name);
		shrtctx.pages.put(name, page);
		return page;
	}
	
	public final PageType pageconfig(String datatype) {
		return shrtctx.pagetype(datatype, datatype, datatype, true);
	}
	
	public Target target(DataType type, String name) {
		var target = targets.get(name);
		if (target == null)
			targets.put(name, target = new Target(type, name));
		return target;
	}
	
	public final TypeContext typectx() {
		return typectx;
	}
}
