package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.GeneralException;

public class CauldronFunction implements Function {
	private Map<String, String> inputs;
	private String output, name;
	private FunctionRule rule;
	private boolean passthrough;
	private int blockmode;
	
	public CauldronFunction(String name) {
		inputs = new HashMap<>();
		this.name = name;
	}
	
	@Override
	public final void beginblock() {
		blockmode = 1;
	}
	
	@Override
	public final void config(DataObject functioncfg) {
		var finput = functioncfg.getItem("input");
		for (var key : inputs.keySet()) {
			var input = finput.instance();
			input.set("name", key);
			input.set("type", inputs.get(key));
		}
		
		functioncfg.set("block_mode", blockmode);
		functioncfg.set("passthrough", passthrough);
		functioncfg.set("output", output);
	}
	
	@Override
	public final void endblock() {
		blockmode = -1;
	}
	
	@Override
	public final void input(String type, String name) {
		inputs.put(name, type);
	}
	
	@Override
	public final void passthrough() {
		passthrough = true;
	}
	
	@Override
	public final void output(String type) {
		output = type;
	}
	
	@Override
	public final void rule(FunctionRule rule) {
		this.rule = rule;
	}
	
	@Override
	public final DataObject run(SharedSession shsession) {
		var session = new Session(shsession);
		if (rule == null)
			throw new GeneralException(
					"no rule defined for function '%s'.", name);
		rule.execute(session);
		return session.output();
	}
}

