package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.application.DataObject;

public interface InputDataObjects {
	
	public abstract DataObject get(String key);
	
}

