package org.quanticsoftware.automata.runtime.functions;

import org.quanticsoftware.automata.application.DataType;
import org.quanticsoftware.automata.application.GetLeastType;
import org.quanticsoftware.automata.runtime.Facility;

public class Passthrough {
	
	public static final void execute(Facility facility, DataType type) {
		var tname = GetLeastType.get(type).getPath();
		var fname = String.format("%s_passthrough", tname);
		
		if (facility.functions().contains(fname))
			return;
		
		var function = facility.function(fname);
		function.input(tname, "input");
		function.output(tname);
		function.passthrough();
		function.rule((s)->s.set(s.input.get("input")));
	}

}
