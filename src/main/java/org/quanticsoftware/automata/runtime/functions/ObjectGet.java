package org.quanticsoftware.automata.runtime.functions;

import org.quanticsoftware.automata.application.DataType;
import org.quanticsoftware.automata.application.GeneralException;
import org.quanticsoftware.automata.runtime.Facility;

public class ObjectGet {
	
	public static final void execute(Facility facility, DataType type) {
		var ikey = type.getKey();
		if ((ikey == null) || type.isCollection())
			return;
		
		var tname = type.getName();
		var fname = new StringBuilder(tname).append("_get").toString();
		var function = facility.function(fname);
		var itype = type.get(ikey).getPath();
		
		function.input(itype, ikey);
		function.output(type.getPath());
		function.rule((s)->{
			var key = s.input.get(ikey).get();
			
			var object = s.dbsession.get(type, key);
			if (object == null)
				fail("no result for '%s' in '%s'.", key.toString(), tname);
			
			s.set(object);
		});
		
	}
	
	private static final void fail(String text, Object... args) {
		throw new GeneralException(text, args);
	}

}
