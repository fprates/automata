package org.quanticsoftware.automata.tests;

import org.quanticsoftware.automata.output.PageContext;
import org.quanticsoftware.automata.runtime.PageConfig;

public class BasePageConfig implements PageConfig {
	
	@Override
	public void execute(PageContext pagectx) {
		var element = pagectx.get("content-httpequiv");
		element.attribs.put("http-equiv", "Content-Type");
		element.attribs.put("content", "text/html; charset=utf-8");
		
		element = pagectx.get("viewport");
		element.attribs.put("name", "viewport");
		element.attribs.put("content",
				"width=device-width, initial-scale=1, shrink-to-fit=no");
		
		element = pagectx.get("js");
		element.text = "";
		element.attribs.put("type", "text/javascript");
		element.attribs.put("src", "/automata/script.js");
		
		pagectx.get("back_listener").engine = "back_listener";
	}
}
