package org.quanticsoftware.automata.tests;

import org.quanticsoftware.automata.application.ContextEntry;
import org.quanticsoftware.automata.runtime.RuntimeContext;
import org.quanticsoftware.automata.runtime.connector.MongoConnector;
import org.quanticsoftware.automata.tests.exhandler.ExHandlerConfig;
import org.quanticsoftware.automata.tests.exhandler.ExHandlerSpec;

public class SystemFacility {

	public static final void define(RuntimeContext context) {
		var typectx = context.typectx();
		
		/*
		 * tipos
		 */
		var pagechanged = typectx.define("page_changed");
		pagechanged.addst("page");
		
		/*
		 * funções
		 */
		var facility = context.facility("system");
		var function = facility.function("page_back");
		function.input("context_entry", "ctxentry");
		function.output("page_changed");
		function.rule((s)->{
			var ctxentry = (ContextEntry)s.input.get("ctxentry").get("entry");
			s.output().set("page", ctxentry.popPage());
		});
		
		function = facility.function("catalog_get");
		function.output("context");
		function.rule((s)->s.set(context.datactx.get("context")));
		
		/*
		 * alvos
		 */
		var ctxentry = new ContextEntry();
		ctxentry.pushPage("page1");
		ctxentry.pushPage("page2");
		
//		var target = context.target("back");
//		var tstctx = target.input(typectx.get("context_entry"), "tstctxentry");
//		tstctx.set("entry", ctxentry);
//		var rules = target.instance(pagechanged);
//		rules.instance("page").rule = (t, o)->o.getst().equals("page1");
		
		var test = context.target(typectx.get("context"), "catalog").test();
		test.rule = (t,o)->o.getbl("active") == true;
		
		/*
		 * páginas
		 */
		context.pagespectemplate = new BasePageSpec();
		context.pageconfigtemplate = new BasePageConfig();
		
		var page = context.page("exhandler");
		page.spec = new ExHandlerSpec();
		page.config = new ExHandlerConfig();

		context.engines.put("exception", new ExceptionEngine());
		context.engines.put("message", new MessageEngine());
		context.engines.put("back_listener", new BackListenerEngine());
		
		pagegenerate(context, "context");
		
//		context.initconn =
//				()->new MongoConnector("localhost", 27017, "automata");
	}
	
	public static final void pagegenerate(RuntimeContext context, String type) {
		var page = context.page("object_display_".concat(type));
		page.spec = (p)->{
			context.pagespectemplate.execute(p);
			p.tree("main", "output_object");
		};
		
		page.config = (p)->{
			context.pageconfigtemplate.execute(p);
			p.get("output_object").datatype = type;
		};
		
		context.outputpages.put(type, page.getName());
	}
}