package org.quanticsoftware.automata.tests;

import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;
import org.quanticsoftware.automata.output.engines.AbstractRenderEngine;
import org.quanticsoftware.automata.output.engines.RenderEngine;

public class BackListenerEngine extends AbstractRenderEngine {

	public BackListenerEngine() {
		super("script", RenderEngine.NO_ID);
		delayrender = true;
	}

	@Override
	protected void execute(RenderContext renderctx, XMLElement output) {
		var sb = new StringBuilder("automata_back_listener_add('").
				append(renderctx.updtarea).append("', '").
				append(renderctx.indexpage).append("', '").
				append(renderctx.currentform).append("');");
		output.addInner(sb.toString());
	}
	
}
