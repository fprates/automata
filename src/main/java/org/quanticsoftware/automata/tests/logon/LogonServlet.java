package org.quanticsoftware.automata.tests.logon;

import javax.servlet.annotation.WebServlet;

import org.quanticsoftware.automata.runtime.RuntimeContext;
import org.quanticsoftware.automata.runtime.Session;
import org.quanticsoftware.automata.servlet.AbstractApplicationServlet;
import org.quanticsoftware.automata.tests.SystemFacility;

@WebServlet("/logon/index.html")
public class LogonServlet extends AbstractApplicationServlet {
	private static final long serialVersionUID = -5333247019799947643L;
	
	@Override
	protected final String init(RuntimeContext context) {
		SystemFacility.define(context);
		
		var typectx = context.typectx();
		var logon = typectx.define("logon");
		logon.addst("username");
		logon.addst("password");
		
		var connuser = typectx.define("connected_user");
		
		/*
		 * system
		 */
		var facility = context.facility("system");
		var function = facility.function("session_connect");
		function.input("logon", "logon");
		function.output("connected_user");
		function.rule((s)->logon(s));
		
		/*
		 * alvos
		 */
		var target = context.target(connuser, "logon");
		target.input(logon, "user_data");
		
		var test = target.test();
		var userdata = test.input("user_data");
		userdata.set("username", "testuser");
		userdata.set("password", "test");
		
		/*
		 * páginas
		 */
		var page = context.page("logon");
		page.spec = new LogonSpec();
		page.config = new LogonConfig();
		
//		context.initconn =
//				()->new MongoConnector("localhost", 27017, "automata");
		return "logon";
	}
	
	private final void logon(Session session) {
		var logon = session.input.get("logon");
		if (!logon.getst("username").equals("testuser"))
			session.error("invalid.username.password");
		if (!logon.getst("password").equals("test"))
			session.error("invalid.username.password");
	}
}
