package org.quanticsoftware.automata.tests.exhandler;

import org.quanticsoftware.automata.output.PageContext;
import org.quanticsoftware.automata.tests.BasePageConfig;

public class ExHandlerConfig extends BasePageConfig {
	
	@Override
	public final void execute(PageContext pagectx) {
		super.execute(pagectx);
		pagectx.get("title").engine = "exception";
		pagectx.get("stacktrace").engine = "exception";
		pagectx.get("message").engine = "message";
	}
}
