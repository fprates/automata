package org.quanticsoftware.automata.tests;

import org.quanticsoftware.automata.output.PageContext;
import org.quanticsoftware.automata.runtime.PageSpec;

public class BasePageSpec implements PageSpec {
	
	@Override
	public void execute(PageContext pagectx) {
		pagectx.view();
		pagectx.meta("content-httpequiv");
		pagectx.meta("viewport");
		pagectx.script("js");
		pagectx.script("back_listener");
		pagectx.form("main");
	}
}
