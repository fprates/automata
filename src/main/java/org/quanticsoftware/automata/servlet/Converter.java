package org.quanticsoftware.automata.servlet;

import java.text.DateFormat;

import org.quanticsoftware.automata.application.ContextEntry;

public class Converter {

	private static final boolean isInitial(String value) {
	    return (value == null)? true : (value.trim().length() == 0);
	}
	
	public static final Object runBoolean(ContextEntry ctxentry, String value) {
		return isInitial(value)?
				false : value.equals("on") || value.equals("true");
	}
	
	public static final Object runByte(ContextEntry ctxentry, String value) {
		return Byte.valueOf(runNumber(value));
	}
	
	public static final Object runDate(ContextEntry ctxentry, String value)
			throws Exception {
	    return (isInitial(value))? null : DateFormat.getDateInstance(
	    		DateFormat.MEDIUM, ctxentry.locale).parse(value);
	}
	
	public static final Object runDouble(ContextEntry ctxentry, String value) {
		return Double.valueOf(runNumber(value));
	}
	
	public static final Object runFloat(ContextEntry ctxentry, String value) {
		return Float.valueOf(runNumber(value));
	}
	
	public static final Object runInt(ContextEntry ctxentry, String value) {
		return Integer.valueOf(runNumber(value));
	}
	
	public static final Object runLong(ContextEntry ctxentry, String value) {
		return Long.valueOf(runNumber(value));
	}
	
	private static final String runNumber(String value) {
		return isInitial(value)? "0" : value;
	}
	
	public static final Object runShort(ContextEntry ctxentry, String value) {
		return Long.valueOf(runNumber(value));
	}
	
	public static final Object runString(ContextEntry ctxentry, String value) {
		return value;
	}
	
	public static final Object runTime(ContextEntry ctxentry, String value)
			throws Exception {
	    return (isInitial(value))? null : DateFormat.getTimeInstance(
	    		DateFormat.MEDIUM, ctxentry.locale).parse(value);
	}
}
