package org.quanticsoftware.automata.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.quanticsoftware.automata.application.ContextEntry;
import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.DataType;
import org.quanticsoftware.automata.application.ErrorMessageException;
import org.quanticsoftware.automata.application.GeneralException;
import org.quanticsoftware.automata.application.GetString;
import org.quanticsoftware.automata.core.CoreFacility;
import org.quanticsoftware.automata.output.PageElement;
import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;
import org.quanticsoftware.automata.runtime.PageType;
import org.quanticsoftware.automata.runtime.Runtime;
import org.quanticsoftware.automata.runtime.RuntimeContext;
import org.quanticsoftware.automata.runtime.SuspendProgramException;
import org.quanticsoftware.automata.runtime.connector.Query;

public abstract class AbstractApplicationServlet extends HttpServlet {
	private static final long serialVersionUID = 5315949165230855110L;
	private Runtime runtime;
	private RuntimeContext rtctx;
	private Map<String, ContextEntry> ctxentries;
	private String initaction;
	private Map<String, DataConverter> converters;
	
	public AbstractApplicationServlet() {
		ctxentries = new HashMap<>();
		converters = new HashMap<>();
		converters.put("boolean", (c, v)->Converter.runBoolean(c, v));
		converters.put("byte", (c, v)->Converter.runByte(c, v));
		converters.put("date", (c, v)->Converter.runDate(c, v));
		converters.put("double", (c, v)->Converter.runDouble(c, v));
		converters.put("float", (c, v)->Converter.runFloat(c, v));
		converters.put("int", (c, v)->Converter.runInt(c, v));
		converters.put("long", (c, v)->Converter.runLong(c, v));
		converters.put("short", (c, v)->Converter.runShort(c, v));
		converters.put("string", (c, v)->Converter.runString(c, v));
		converters.put("time", (c, v)->Converter.runTime(c, v));
	}
	
	private final Object convert(
			ContextEntry ctxentry,
			DataType type,
			String value) throws Exception {
		var stype = type.getSuperType();
		if (stype != null)
			type = stype;
		
		return converters.get(type.getName()).run(ctxentry, value);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			run(req, resp, true);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		try {
			run(req, resp, false);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

	private final String execute(RuntimeContext context) {
		var typectx = context.typectx();
		typectx.define("context_entry").add(typectx.get("any"), "entry");
		
		var numobjtype = typectx.define(null, "number_object", "type");
		numobjtype.addst("type");
		numobjtype.addl("current");
		
		context.facility("core", new CoreFacility());
		
		return init(context);
	}
	
	private final Map<String, String[]> extract(
			Map<String, FileStatus> files,
			HttpServletRequest req) throws Exception {
	    files.clear();
	    
	    if (!ServletFileUpload.isMultipartContent(req))
	        return req.getParameterMap();
	    
	    var fileupload = new ServletFileUpload(new DiskFileItemFactory());
	    for (var fileitem : (List<FileItem>)fileupload.parseRequest(req))
	        files.put(fileitem.getFieldName(), new FileStatus(fileitem));
	    
	    return processMultipartContent(files);
	}
	
	private final void fail(String text, Object... args) {
		throw new GeneralException(text, args);
	}
	
	private final void generateKey(ContextEntry ctxentry, DataObject data) {
		var type = data.getType();
		var objname = type.getPath();
		var numobjtype = rtctx.typectx().get("number_object");
		var numobj = ctxentry.dbsession.get(numobjtype, objname);
		long id = (numobj == null)? 1 : numobj.getl("current") + 1;
		
		if (id == 1) {
			numobj = ctxentry.instance(numobjtype, "numobj");
			numobj.set("type", objname);
			numobj.set("current", id);
			ctxentry.dbsession.insert(numobj);
		} else {
			var query = new Query();
			query.values("current", id);
			query.setType(numobj.getType());
			query.andEqual("type", objname);
			ctxentry.dbsession.update(query);
		}
		/*
		 * we have quite a lot of prereq once we want to automatically
		 * generate key values to children:
		 * 1. if must be a reference item (in other words, non allocable)
		 * 2. item of a object can't be null
		 * 3. item must have a key element
		 * 4. key element of the object must be null.
		 */
		data.set(type.getKey(), id);
		for (var key : type.getItems()) {
			if (type.isAllocable(key))
				continue;
			
			var item = data.getItem(key);
			if (item == null)
				continue;
			
			var ikey = item.getType().getKey();
			if (ikey == null)
				continue;
			
			if (item.get(ikey) != null)
				continue;
			
			generateKey(ctxentry, item);
		}
	}
	
	private final PageTransform getPage(DataObject object, String outputpage) {
		var typename = object.getType().getPath();
		var pagetype = runtime.shrtctx.pagetypes.get(typename).get(outputpage);
		if (pagetype != null)
			return new PageTransform(pagetype, object.getName());
		
		for (var key : object.getItems()) {
			var item = object.getItem(key);
			if (item == null)
				continue;
			
			var page = getPage(item, outputpage);
			if (page != null)
				return page;
		}
		
		return null;
	}
	
	protected abstract String init(RuntimeContext context);
	
	private final Runtime initialize() {
		var runtime = new Runtime();
		initaction = runtime.init((c)->execute(c));
		
		rtctx = runtime.context();
		if ((initaction != null) && !rtctx.targets.containsKey(initaction))
			fail("target '%s' is not registered.",
					(initaction == null)? "null" : initaction);
		
		rtctx.eventhandlers.put("missing_input", new MissingInput(runtime));
		return runtime;
	}
	
	private final void parameters2context(
			ContextEntry ctxentry,
			Map<String, String[]> parameters) throws Exception {
		for (var key : ctxentry.inputs) {
		    var value = GetString.run(parameters, key);
		    
		    var entry = ctxentry.objects.get(key);
		    if (entry == null)
		        fail("no object found for input '%s'.", key);
		    
		    entry.set(convert(ctxentry, entry.getType(), value));
		//            if (filehandler != null)
		//                filehandler.run(ctxentry, files, key);
		}
	}
	
	private final void print(HttpServletResponse resp, ContextEntry ctxentry)
			throws Exception {
		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
		
		var writer = resp.getWriter();
		writer.print(ctxentry.output);
		writer.flush();
		writer.close();
	}
	
	private final Map<String, String[]> processMultipartContent(
	        Map<String, FileStatus> files) throws Exception {
		var parameters = new HashMap<String, String[]>();
		
		for (var fkey : files.keySet()) {
		    var filestatus = files.get(fkey);
		    if (!filestatus.item.isFormField()) {
		        parameters.put(fkey, new String[] {filestatus.item.getName()});
		        filestatus.file = true;
		        continue;
		    }
		    
		    var value = filestatus.item.getString("UTF-8");
		    var values = parameters.get(fkey);
		    if ((values == null) || (values != null && value.length() > 0))
		        parameters.put(fkey, new String[] {value});
		}
		
		return parameters;
	}
	
	private final void raiseev(
			String event,
			ContextEntry ctxentry,
			Object value) {
		var handler = rtctx.eventhandlers.get(event);
		if (handler != null)
			handler.run(ctxentry, value);
	}
	
	private final XMLElement render(RenderContext renderctx, String name) {
		renderctx.element = renderctx.page.get(name);
		if (renderctx.element.disabled)
			return null;
		
		var enginename = (renderctx.element.engine == null)?
				renderctx.element.type : renderctx.element.engine;
		
		var engine = rtctx.engines.get(enginename);
		if (engine == null)
			fail("no engine registered for '%s'.", enginename);
		
		if ((renderctx.updtarea == null) && !engine.hasNoPrintedId())
			renderctx.updtarea = name;
		
		var output = engine.run(renderctx);
		for (var child : renderctx.element.items) {
			var coutput = render(renderctx, child);
			if (coutput != null)
				output.addChild(coutput);
		}
		
		return output;
	}
	
	private final String render(ContextEntry ctxentry, String pagename) {
		var page = runtime.shrtctx.pages.get(pagename);
		var renderctx = RenderContext.instance(ctxentry);
		renderctx.page = runtime.build(page);
		renderctx.typectx = rtctx.typectx();
		
		if (!ctxentry.reload && (renderctx.updtarea != null)) {
			var output = render(renderctx, renderctx.updtarea);
			renderDelayed(renderctx);
			return output.toString();
		}
		
		renderctx.updtarea = renderctx.page.getResponseElement(renderctx.action);
		var html = render(renderctx, renderctx.page.getRoot());
		renderDelayed(renderctx);
		html.head("<!DOCTYPE html>");
		return html.toString();
	}
	
	private final void renderDelayed(RenderContext renderctx) {
		renderctx.laterendering = true;
		for (var delayed : renderctx.delayed) {
			renderctx.element = (PageElement)delayed[0];
			renderctx.output = (XMLElement)delayed[1];
			render(renderctx, renderctx.element.name);
		}
	}
	
	private void run(
			HttpServletRequest req,
			HttpServletResponse resp,
			boolean reload) throws Exception {
		boolean ctxinit;
		if (runtime == null)
			runtime = initialize();
		
		var sessionid = req.getSession().getId();
		
		var ctxentry = ctxentries.get(sessionid);
		if (ctxentry == null) {
			ctxentries.put(sessionid, ctxentry = new ContextEntry());
			ctxentry.locale = new Locale("pt", "BR");
			runtime.setConnector(ctxentry);
			
			var ctxobj = ctxentry.instance(
					rtctx.typectx().get("context_entry"),
					"context_entry");
			
			ctxobj.set("entry", ctxentry);
			ctxentry.add(ctxobj);
			ctxinit = true;
		} else {
			ctxentry.msgtext = null;
			ctxentry.msgargs = null;
			ctxentry.msgstatus = null;
			ctxinit = false;
		}

		var files = new HashMap<String, FileStatus>();
		var parameters = extract(files, req);
		parameters2context(ctxentry, parameters);
		ctxentry.action = GetString.run(parameters , "action");
		if ((ctxentry.action == null) && ctxinit)
			ctxentry.action = initaction;
		
		ctxentry.indexpage = req.getRequestURI();
		ctxentry.reload = reload;
		req.setCharacterEncoding("UTF-8");
		if (ctxentry.otarget == null)
			ctxentry.otarget = ctxentry.action;
		
		try {
			if (!ctxentry.dbsession.isTransactionStarted())
				ctxentry.dbsession.start();
			
			if (ctxentry.action != null)
				run(ctxentry);
			
			var page = ctxentry.getPage();
			if (page != null)
				ctxentry.output = render(ctxentry, page);
			
			ctxentry.dbsession.commit();
			ctxentry.otarget = null;
		} catch (ErrorMessageException e) {
			ctxentry.dbsession.rollback();
			ctxentry.msgtext = e.text;
			ctxentry.msgargs = e.args;
			ctxentry.msgstatus = "error";
			ctxentry.output = render(ctxentry, ctxentry.getPage());
		} catch (SuspendProgramException e) {
			ctxentry.suspendstate = e.state;
			raiseev(e.state.reason, ctxentry, e.state);
			ctxentry.output = render(ctxentry, ctxentry.getPage());
		} catch (Exception e) {
			ctxentry.output = null;
			ctxentry.ex = e;
			ctxentry.fail = true;
			ctxentries.remove(sessionid);
			ctxentry.dbsession.rollback();
			ctxentry.dbsession.close();
			if (!runtime.shrtctx.pages.containsKey("exhandler"))
				throw e;
			
			ctxentry.output = render(ctxentry, "exhandler");
		} finally {
			if (ctxentry.fail && (ctxentry.output == null))
				throw ctxentry.ex;
			if (ctxentry.output != null)
				print(resp, ctxentry);
		}
	}
	
	private final DataObject run(ContextEntry ctxentry, String tname)
			throws Exception {
		var target = rtctx.targets.get(tname);
		if (target == null)
			fail("target '%s' not registered.", tname);
		
		for (var rtarget : target.required) {
			if (ctxentry.vtargets.contains(rtarget))
				continue;
			
			if (run(ctxentry, rtarget) == null)
				return null;
			
			ctxentry.vtargets.add(rtarget);
		}
		
		var program = rtctx.programs.get(tname);
		if (program == null)
		    fail("failed retrieving program for '%s'.", tname);
		
		ctxentry.suspendstate = null;
		return runtime.run(ctxentry.dbsession, program, ctxentry.objects);
	}
	
	private final void run(ContextEntry ctxentry) throws Exception {
		var object = run(ctxentry, ctxentry.action);
		ctxentry.add(object);
		
		var outputpage = rtctx.outputpages.get(object.getType().getPath());
		
		var pagetransform = getPage(object, outputpage);
		if (pagetransform != null) {
			ctxentry.pushPage(pagetransform.pagetype.page());
			ctxentry.alias(pagetransform.pagetype.element(), object);
		}
		
		var type = object.getType();
		if (type.getKey() == null)
			return;
		
		if (type.isAutoGenKey())
			generateKey(ctxentry, object);
		
		ctxentry.dbsession.save(object);
	}
}

class PageTransform {
	public PageType pagetype;
	public String name;
	
	public PageTransform(PageType pagetype, String name) {
		this.pagetype = pagetype;
		this.name = name;
	}
}
