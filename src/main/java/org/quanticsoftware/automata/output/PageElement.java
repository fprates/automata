package org.quanticsoftware.automata.output;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class PageElement {
	public String type, parent, name, text, action, engine, datatype;
	public Object[] args;
	public boolean submit, secret, disabled, notypeindexing;
	public Set<String> items;
	public Map<String, String> attribs;
	
	public PageElement(String type, String parent, String name) {
		this.type = type;
		this.parent = parent;
		this.name = name;
		items = new LinkedHashSet<>();
		attribs = new HashMap<>();
	}
}
