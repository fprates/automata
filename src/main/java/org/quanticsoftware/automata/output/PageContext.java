package org.quanticsoftware.automata.output;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class PageContext {
	private Map<String, PageElement> elements;
	private String root;
	private Map<String, Messages> translations;
	private Map<String, String> respelements;
	
	public PageContext() {
		elements = new HashMap<>();
		translations = new HashMap<>();
		respelements = new HashMap<>();
	}
	
	public final void addResponseElement(String action, String element) {
		respelements.put(action, element);
	}
	
	public final PageElement element(String type, String parent, String name) {
		var element = new PageElement(type, parent, name);
		elements.put(name, element);
		if (parent != null)
			get(parent).items.add(name);
		if (root == null)
			root = name;
		return element;
	}
	
	public final PageElement button(String parent, String name) {
		return element("button", parent, name);
	}
	
	public final PageElement container(String parent, String name) {
		return element("container", parent, name);
	}
	
	public final PageElement dataform(String parent, String name) {
		return element("dataform", parent, name);
	}
	
	public final Set<String> elements() {
		return elements.keySet();
	}
	
	public final PageElement form(String name) {
		return element("form", "body", name);
	}
	
	public final PageElement get(String name) {
		return elements.get(name);
	}
	
	public final Messages getMessages(String locale) {
		var messages = translations.get(locale);
		if (messages == null)
		    translations.put(locale, messages = new Messages());
		return messages;
	}
	
	public final String getResponseElement(String action) {
		return respelements.get(action);
	}
	
	public final String getRoot() {
		return root;
	}
	
	public final PageElement meta(String name) {
		return element("meta", "head", name);
	}
	
	public final PageElement nodelist(String parent, String name) {
		return element("node_list", parent, name);
	}
	
	public final PageElement nodelistitem(String parent, String name) {
		return element("node_list_item", parent, name);
	}
	
	public final PageElement script(String name) {
		return element("script", "head", name);
	}
	
	public final PageElement text(String parent, String name) {
		return element("text", parent, name);
	}
	
	public final PageElement textfield(String parent, String name) {
		return element("textfield", parent, name);
	}
	
	public final PageElement tree(String parent, String name) {
		return element("tree", parent, name);
	}
	
	public final PageElement view() {
		var view = element("view", null, "view");
		element("head", "view", "head");
		element("body", "view", "body");
		return view;
	}
}
