package org.quanticsoftware.automata.output.engines;

import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;

public class TextEngine extends AbstractRenderEngine {

	public TextEngine() {
		super("p");
	}

	@Override
	protected void execute(RenderContext renderctx, XMLElement output) {
		if (renderctx.element.datatype != null)
			output.addInner(toString(renderctx, renderctx.element));
		else if (renderctx.element.text != null)
			output.addInner(toString(renderctx, renderctx.element.text));
		else
			output.addInner(toString(renderctx, renderctx.element.name));
	}
	
}
