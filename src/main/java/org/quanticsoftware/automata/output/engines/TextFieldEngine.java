package org.quanticsoftware.automata.output.engines;

import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;

public class TextFieldEngine extends AbstractRenderEngine {

	public TextFieldEngine() {
		super("input");
	}

	@Override
	protected void execute(RenderContext renderctx, XMLElement output) {
		instance(renderctx, renderctx.element);
		output.add("name", renderctx.element.name);
		output.add("type", renderctx.element.secret? "password" : "text");
		output.add("value", toString(renderctx, renderctx.element));
		renderctx.input(renderctx.element.name);
	}
	
}
