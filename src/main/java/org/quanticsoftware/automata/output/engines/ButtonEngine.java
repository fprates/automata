package org.quanticsoftware.automata.output.engines;

import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;

public class ButtonEngine extends AbstractRenderEngine {

	public ButtonEngine() {
		super("button");
	}

	@Override
	protected void execute(RenderContext renderctx, XMLElement output) {
		output.add("type", (renderctx.element.submit)? "submit" : "button");
		output.add("name", renderctx.element.name);
		output.add("data-automata-form", renderctx.currentform);
		var action = (renderctx.element.action == null)?
				renderctx.element.name : renderctx.element.action;
		output.add("data-automata-action", action);
		output.add("data-automata-index", renderctx.indexpage);
		var updtarea = renderctx.page.getResponseElement(action);
		if (updtarea == null)
			updtarea = renderctx.updtarea;
		output.add("data-automata-updtarea", updtarea);
		output.addInner(toString(
				renderctx, (renderctx.element.text == null)?
						renderctx.element.name : renderctx.element.text));
	}
	
}
