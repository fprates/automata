package org.quanticsoftware.automata.output.engines;

import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.output.PageElement;
import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;

public class TreeEngine extends AbstractRenderEngine {

	public TreeEngine() {
		super("div");
	}
	
	private final void build(
			RenderContext renderctx, PageElement widget, DataObject data) {
		var list = renderctx.page.nodelist(widget.name, data.getPath());
		var item = renderctx.page.
				nodelistitem(list.name, list.name.concat("_name"));
		item.text = toString(renderctx, data.getName());
		
		if (data.get() != null) {
			item = renderctx.page.
					nodelist(list.name, list.name.concat("_item"));
			item = renderctx.page.
					nodelistitem(item.name, item.name.concat("_value"));
			item.text = toString(renderctx, data);
		}
		
		for (String key : data.getItems()) {
			var child = data.getItem(key);
			if (child != null)
				build(renderctx, list, child);
		}
	}
	
	@Override
	protected void execute(RenderContext renderctx, XMLElement output) {
		var data = instance(renderctx, renderctx.element);
		build(renderctx, renderctx.element, data);
	}
	
}
