package org.quanticsoftware.automata.output.engines;

import org.quanticsoftware.automata.output.PageElement;
import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;

public class DataFormEngine extends AbstractRenderEngine {
	
	public DataFormEngine() {
		super("div");
	}

	private final PageElement builditem(BuildItemContext itemctx) {
		var fieldname = (itemctx.suffix == null)?
				itemctx.field :
					itemctx.field.concat("_").concat(itemctx.suffix);
		var name = new StringBuilder(itemctx.renderctx.element.name).
				append(".").append(fieldname).toString();
		var refitem = itemctx.renderctx.page.get(name);
		var item = itemctx.renderctx.page.
				element(itemctx.type, itemctx.parent, name);
		if (refitem != null) {
			copy(item, refitem);
			itemctx.renderctx.element.items.remove(name);
		}
		return item;
	}
	
	private final void copy(PageElement to, PageElement from) {
		to.datatype = from.datatype;
		to.submit = from.submit;
		to.secret = from.secret;
		to.disabled = from.disabled;
		to.text = from.text;
		to.items.addAll(from.items);
	}
	
	@Override
	protected final void execute(RenderContext renderctx, XMLElement output) {
		var itemctx = new BuildItemContext();
		itemctx.renderctx = renderctx;
		var datatype = instance(renderctx, renderctx.element).getType();
		for (var key : datatype.getItems()) {
			itemctx.parent = renderctx.element.name;
			itemctx.field = key;
			itemctx.suffix = "item";
			itemctx.type = "container";
			var item = builditem(itemctx);
			var itemname = item.name;
			
			itemctx.parent = itemname;
			itemctx.suffix = itemctx.type = "text";
			item = builditem(itemctx);
			
			itemctx.suffix = null;
			itemctx.type = "textfield";
			item = builditem(itemctx);
			item.datatype = datatype.get(key).getPath();
		}
	}
}

class BuildItemContext {
	public RenderContext renderctx;
	public String parent, field, suffix, type;
}
