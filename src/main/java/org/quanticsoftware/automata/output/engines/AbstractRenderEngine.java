package org.quanticsoftware.automata.output.engines;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.application.DataObject;
import org.quanticsoftware.automata.application.DataType;
import org.quanticsoftware.automata.output.PageElement;
import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;

public abstract class AbstractRenderEngine implements RenderEngine {
	private String tag;
	private boolean noprintedid;
	private Map<String, Conversion> conversions;
	private DateFormat dtformat;
	protected boolean delayrender;
	
	public AbstractRenderEngine(String tag) {
		this(tag, false);
	}
	
	public AbstractRenderEngine(String tag, boolean noprintedid) {
		this.tag = tag;
		this.noprintedid = noprintedid;
		conversions = new HashMap<>();
		conversions.put("date", (c, v)->date(c, v));
	}
	
	private final String date(RenderContext renderctx, Object value) {
		if (dtformat == null)
		    dtformat = DateFormat.getDateInstance(
		            DateFormat.SHORT, renderctx.locale);
		return (value == null)? "" : dtformat.format(value);
	}
	
	protected abstract void execute(RenderContext renderctx, XMLElement output);
	
	@Override
	public final boolean hasNoPrintedId() {
		return noprintedid;
	}
	
	protected final DataObject instance(
			RenderContext renderctx, PageElement element) {
		var object = renderctx.objects.get(element.name);
		if (object != null)
			return object;
		var datatype = renderctx.typectx.get(element.datatype);
		object = renderctx.datactx.instance(datatype, element.name);
		renderctx.add(object);
		return object;
	}
	
	@Override
	public final boolean isRenderingDelayed() {
		return delayrender;
	}
	
	@Override
	public final XMLElement run(RenderContext renderctx) {
		XMLElement output;
		
		if (delayrender) {
			if (!renderctx.laterendering) {
				output = new XMLElement(tag);
				renderctx.delayed.add(new Object[] {renderctx.element, output});
				return output;
			}
			output = renderctx.output;
		} else {
			output = new XMLElement(tag);
		}
		
		if (!noprintedid)
			output.add("id", renderctx.element.name);
		for (var key : renderctx.element.attribs.keySet())
			output.add(key, renderctx.element.attribs.get(key));
		execute(renderctx, output);
		return output;
	}
	
	protected final String toString(
			RenderContext renderctx, String text, Object... args) {
		if ((text == null) || (renderctx.locale == null))
		    return null;
		var translation = renderctx.page.getMessages(
				renderctx.locale.toString()).get(text);
		var output = (translation == null)? text : translation;
		return String.format(output, args);
	}
	
	protected final String toString(RenderContext renderctx, DataObject data) {
		var value = data.get();
		var type = data.getType();
		return toString(renderctx, type, value);
	}
	
	protected final String toString(
			RenderContext renderctx, PageElement element) {
		var type = renderctx.typectx.get(element.datatype);
		var object = renderctx.objects.get(element.name);
		var value = (object == null)? null : object.get();
		return toString(renderctx, type, value);
	}
	
	private final String toString(
			RenderContext renderctx, DataType type, Object value) {
		if (type == null)
		    return (value == null)? "" : value.toString();
		var supertype = type.getSuperType();
		type = (supertype == null)? type : supertype;
		var conversion = conversions.get(type.getPath());
		if (conversion == null)
		    return (value == null)? "" : value.toString();
		return conversion.toString(renderctx, value);
	}
	
}

interface Conversion {
    
    public abstract String toString(RenderContext renderctx, Object value);
}

