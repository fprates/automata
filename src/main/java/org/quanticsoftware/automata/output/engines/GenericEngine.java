package org.quanticsoftware.automata.output.engines;

import org.quanticsoftware.automata.output.RenderContext;
import org.quanticsoftware.automata.output.XMLElement;

public class GenericEngine extends AbstractRenderEngine {
	
	public GenericEngine(String tag) {
		super(tag, false);
	}
	
	public GenericEngine(String tag, boolean printid) {
		super(tag, printid);
	}
	
	@Override
	protected final void execute(RenderContext renderctx, XMLElement output) {
		if (renderctx.element.text != null)
			output.addInner(toString(renderctx, renderctx.element.text));
	}
}
